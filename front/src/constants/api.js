/**
 * API CONSTANTS
 * */

 export default {
    getItems:                              {   endpoint: '/product/getAll',                                       method: 'GET'                      },
    getItemsByPrice:                       {   endpoint: '/product/getAllByPrice',                                method: 'GET'                      },
    getSections:                           {   endpoint: '/category/getAll',                                      method: 'GET'                      },  
    search:                                {   endpoint: '/product/search',                                       method: 'POST'                      },     
};
